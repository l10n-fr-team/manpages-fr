#!/usr/bin/perl

# Updates automatically strings for dates, then unfuzzy these strings
# Input: po file on stdin
# Output: stdout
# Simon Paillard, 2012

use strict;

# split per paragraph
local $/ = "";

my @Months = ('janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'); 

while ( <> ) {
	if ($_ =~ /^msgid \"(\d*)-(\d\d)-(\d\d)\"/m) {
		my $year = $1 ;
		my $month = $2 ;
		my $day = sprintf("%d", $3) ;
		
		$day = "1er" if ($day == 1);

		my $paragraph = $_;
		$paragraph =~ s/msgstr \".*\"/msgstr "$day $Months[$month-1] $year"/;

		# remove fuzzy mark
		$paragraph =~ s/^(#.*), fuzzy/$1/m;
		$paragraph =~ s/^#, fuzzy\n//m;

		# remove previous msgid
		$paragraph =~ s/^#\| .*\n//m;

		print $paragraph ;
	}
	# Remove old dates strings
	elsif ($_ =~ /^#~ msgid \"(\d*)-(\d\d)-(\d\d)\"/m) {
		next;	
	}
	else {
		print $_;
	}
}
